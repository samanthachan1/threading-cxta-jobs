'''
 Example Python script to concurrently run multiple parametrized Robot Framework tests
 Expects following input files:
    - suite.robot:     Robot Framework test case file
    - user_list.txt:   List of usernames (one per line)
'''

import os
# import process
import subprocess
import yaml

# clean up from last run
baseFolder = 'data_tests'
os.system('rm -rf ' + baseFolder)
os.mkdir(baseFolder)

# no output to console please
FNULL = open(os.devnull, 'w')

with open(r'./device_names.yaml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    device_list = yaml.load(file)['devices']

processes = set()
for device in device_list:
    path = './' + baseFolder + '/' + device
    os.mkdir(path)

    cmd_parts = ['robot', '--variable', 'DEVICE:' + device, '../../test.robot']
    processes.add(subprocess.Popen(cmd_parts, cwd=path, stdout=FNULL))